$(document).ready(function () {

    $('#mdlConductores').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Conductores'},
            {extend: 'pdf', title: 'Conductores'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-right', targets: [3] },
            { className: 'text-center', targets: [4,5] },
        ],
    });

    fnObtenerConductores();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_conductor").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerConductores();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_conductor')>0){
            title="Modificar Conductor";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_conductor'));
        }else{
            title="Nuevo Conductor";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_conductor").val("");
            $("#ln_nombre").val("");
            $("#ln_apellidos").val("");
            $("#dt_fecha_nacimiento").val("");
            $("#ln_direccion").val("");
            $("#ln_telefono").val("");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_conductor'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_conductor = $(this).data('nu_conductor');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_conductor);
    });
    
});

function fnEditarRegistro(nu_conductor){
    var url = 'Conductores/'+nu_conductor;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.conductor !== null){
            $.each(infoData.conductor,function(index, el) {
                $("#nu_conductor").val(el.nu_conductor);
                $("#ln_nombre").val(el.ln_nombre);
                $("#ln_apellidos").val(el.ln_apellidos);
                $("#dt_fecha_nacimiento").val(el.dt_fecha_nacimiento);
                $("#ln_direccion").val(el.ln_direccion);
                $("#ln_telefono").val(el.ln_telefono);
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'Conductores';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerConductores();
    }
}

function fnEliminarRegistro(nu_conductor){
    var formData = new FormData();
    var url = 'Conductores/'+nu_conductor;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerConductores();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_conductor){
    var url = 'Conductores/'+nu_conductor;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerConductores();
    }
}

function fnModificarEstatus(nu_conductor,nu_activo){
    var url = 'Conductores/'+nu_conductor;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerConductores(){
    var formData = new FormData();
    formData.append("ln_nombre", $('#txtNombre').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','Conductores',formData);

    if(infoData.intState){
        if(infoData.conductores !== null){
            var estatus="";
            var date = new Date();
            var dateFormat = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate(); 

            var fechaFin = new Date(dateFormat).getTime();
            var fechaInicio =""; 
            var difFechas ="";

            var btnEliminar="";
            var btnModificar="";

            var chkEstatus="";

            if($('#tblConductores').DataTable().data().length > 0){
                $('#tblConductores').DataTable().clear();
                $('#tblConductores').DataTable().draw();
            }

            $.each(infoData.conductores,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_conductor="'+el.nu_conductor+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                fechaInicio = new Date(el.dt_fecha_nacimiento).getTime();
                difFechas = (fechaFin - fechaInicio);

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_conductor="'+el.nu_conductor+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_conductor="'+el.nu_conductor+'"><i class="fa fa-trash"></i></button>';

                $('#tblConductores').dataTable().fnAddData( 
                    [
                        el.ln_nombre +" "+el.ln_apellidos, 
                        el.ln_direccion, 
                        el.ln_telefono, 
                        parseInt((difFechas/(1000*60*60*24) )/365), 
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}

