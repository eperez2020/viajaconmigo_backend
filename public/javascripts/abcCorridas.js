$(document).ready(function () {

    $('#mdlCorridas').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Corridas'},
            {extend: 'pdf', title: 'Corridas'},
            {extend: 'print',
                customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
            }
        ],
        //pageLength : 1000,
        columnDefs: [
            { className: 'text-right', targets: [6] },
            { className: 'text-center', targets: [0, 3,  4, 5, 7, 8] },
        ],
    });

    fnObtenerCorridas();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_corrida").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerCorridas();
    });

    $("#btnAgregarPunto").click(function(){
        fnAgregarPunto();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_corrida')>0){
            title="Modificar Corrida";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_corrida'));
        }else{
            title="Nueva Corrida";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_corrida").val("");
            $('#nu_origen, #nu_llegada').multiselect('rebuild');
            $('#ln_hora_salida').val("");
            $('#ln_hora_llegada').val("");
            $('#dbl_precio').val("");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });

    $('#mdlPuntosReunion').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $("#nu_corrida2").val(button.data('nu_corrida'));
        $("#txtDescripcionCorrida").val(button.data('corrida'));
        fnObtenerPuntos(button.data('nu_corrida'));
    });

});

$(document).on("ifChanged",".chkEstatusPunto",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatusPunto($(this).data('nu_punto'), nu_activo);
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_corrida'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_corrida = $(this).data('nu_corrida');
    swal({
        title: "¿Desea eliminar la corrida?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_corrida);
    });
    
});

function fnAgregarPunto(){
    var url = 'Puntos';
    var formData = new FormData();
    formData.append("nu_corrida", $("#nu_corrida2").val());
    formData.append("ln_nombre", $("#ln_nombre").val());
    formData.append("nu_tipo", $("#nu_tipo").val());
    formData.append("nu_activo", "1");
    formData.append("_method", 'POST');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        fnObtenerPuntos($("#nu_corrida2").val());
    }  
}

function fnObtenerPuntos(nu_corrida){
    var url = 'Puntos';
    var formData = new FormData();

    formData.append("nu_corrida", nu_corrida);
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);
    if(infoData.intState){
        if(infoData.puntos !== null){
            var strHTML="";
            var chkEstatus="";
            var estatus="";

            $.each(infoData.puntos,function(index, el) {

                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" class="i-checks chkEstatusPunto" data-nu_punto="'+el.nu_punto+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                strHTML+="<tr>";
                strHTML+="<td class='text-center'>"+(index + 1)+"</td>";
                strHTML+="<td>"+el.ln_nombre+"</td>";
                strHTML+="<td class='text-center'>"+(el.nu_tipo == '0' ? 'SALIDA' : 'LLEGADA')+"</td>";
                strHTML+="<td class='text-center'>"+estatus+"</td>";
                strHTML+="</tr>";
            });

            $("#tblPuntos tbody").empty();
            $("#tblPuntos tbody").append(strHTML);

            $('#tblPuntos .i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    } 
}

function fnEditarRegistro(nu_corrida){
    var url = 'Corridas/'+nu_corrida;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.corrida !== null){
            $.each(infoData.corrida,function(index, el) {
                $("#nu_corrida").val(el.nu_corrida);
                $('#nu_origen').multiselect('select', el.nu_origen);
                $('#nu_llegada').multiselect('select', el.nu_llegada);
                $('#nu_origen, #nu_llegada').multiselect('refresh');
                $('#ln_hora_salida').val(el.ln_hora_salida);
                $('#ln_hora_llegada').val(el.ln_hora_llegada);
                $('#dbl_precio').val(el.dbl_precio);
                              
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'Corridas';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerCorridas();
    }
}

function fnEliminarRegistro(nu_corrida){
    var formData = new FormData();
    var url = 'Corridas/'+nu_corrida;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerCorridas();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_corrida){
    var url = 'Corridas/'+nu_corrida;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerCorridas();
    }
}

function fnModificarEstatus(nu_corrida,nu_activo){
    var url = 'Corridas/'+nu_corrida;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnModificarEstatusPunto(nu_punto,nu_activo){
    var url = 'Puntos/'+nu_punto;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerCorridas(){
    var formData = new FormData();

    formData.append("nu_origen", $('#cmbOrigen').val());
    formData.append("nu_llegada", $('#cmbDestino').val());
    formData.append("nu_activo", "");
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST','Corridas',formData);

    if(infoData.intState){
        if(infoData.conductores !== null){
            var estatus="";
            var btnEliminar="";
            var btnModificar="";
            var btnPuntos="";
            var chkEstatus="";

            if($('#tblTablaGeneral').DataTable().data().length > 0){
                $('#tblTablaGeneral').DataTable().clear();
                $('#tblTablaGeneral').DataTable().draw();
            }

            $.each(infoData.corridas,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_corrida="'+el.nu_corrida+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_corrida="'+el.nu_corrida+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_corrida="'+el.nu_corrida+'"><i class="fa fa-trash"></i></button>';

                btnPuntos='<button class="btn btn-success btn-xs btnPuntosSalida" type="button" data-toggle="modal" data-target="#mdlPuntosReunion" data-corrida="'+el.nombre_origen+" - "+el.nombre_llegada+'" data-nu_corrida="'+el.nu_corrida+'"><i class="fa fa-map-marker"></i></button>';
                
                
                $('#tblTablaGeneral').dataTable().fnAddData( 
                    [
                        (index + 1),
                        el.nombre_origen,
                        el.nombre_llegada,
                        el.ln_hora_salida,
                        el.ln_hora_llegada,
                        btnPuntos,
                        number_format(el.dbl_precio, 2, '.', ','),
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('#tblTablaGeneral .i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}
