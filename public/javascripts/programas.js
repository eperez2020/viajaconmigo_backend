$(document).ready(function () {

    $('#mdlProgramacion').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Programacion'},
            {extend: 'pdf', title: 'Programacion'},
            {extend: 'print',
                customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
            }
        ],
        //pageLength : 1000,
        columnDefs: [
            { className: 'text-right', targets: [6, 7] },
            { className: 'text-center', targets: [0, 2, 3, 4, 5, 9, 10, 11] }
        ],
    });

    $('#nu_semanas').multiselect({
        enableFiltering: true,
        filterBehavior: 'text',
        enableCaseInsensitiveFiltering: true,
        buttonWidth: '100%',
        numberDisplayed: 1,
        includeSelectAllOption: true
    });

    $('#mdlProgramarFecha').on('shown.bs.modal', function (event) {
        var modal = $(this);
        var strHTML = "";

        var cmbVehiculos ='<select class="form-control selectVehiculos"></select>'

        if($("#tblTablaGeneral").find(".chkSelCorrida").length>0){
            $("#tblProgramarCorridas tbody").empty();
            $("#tblTablaGeneral").find(".chkSelCorrida").each(function(index){
                if( $(this).prop('checked') ) {
                    strHTML+="<tr>";
                    strHTML+="<td>"+(index+1)+"</td>";
                    strHTML+="<td>"+$(this).data('corrida') +"</td>";
                    strHTML+="<td>"+cmbVehiculos+"</td>";
                    strHTML+="</tr>";
                }
            });
            $("#tblProgramarCorridas tbody").append(strHTML);

            var formData = new FormData();
            formData.append("_method", 'GET');
            formData.append("_token", $('input[name="_token"]').attr('value'));
            var infoData = fnAjaxSelect('POST','fnCargarComboVehiculos',formData);

            if(infoData.intState){
                if(infoData.datos !== null){
                    fnListadoSelectGeneral('.selectVehiculos', infoData.datos, 1);
                }
            }

        }else{
            modal.modal('hide');
            return false;
        }
        
    });

    fnObtenerProgramacion();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_programa").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerProgramacion();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_programa')>0){
            title="Modificar Salida";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_programa'));
        }else{
            title="Programar Nueva Salida";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_programa").val("");
            $( "#dt_fecha_inicio, #dt_fecha_fin" ).datepicker({dateFormat:"yy/mm/dd"}).datepicker("setDate",new Date());
            $('#mdlFormulario .selectFormatoGeneral').val('');
            $('#mdlFormulario .selectFormatoGeneral').multiselect('rebuild');
            $("#nu_activo").prop('checked',true).iCheck('update');
            $("#dt_fecha_inicio, #dt_fecha_fin").prop('disabled', false);
        }
        
        modal.find('.modal-title').text('' + title)
    });

    $('#mdlPuntosReunion').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        
        fnObtenerPuntos(button.data('nu_corrida'));
    });

});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_programa'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_programa = $(this).data('nu_programa');
    swal({
        title: "¿Desea eliminar la salida?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_programa);
    });
    
});

function fnCargarCalendario(objFechas){
    
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar
        drop: function() {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        events: objFechas  ,
        eventLimit: true,
        views: {
            agenda: {
              eventLimit: 1 // adjust to 6 only for agendaWeek/agendaDay
            }
          }         

    });
}

function fnObtenerPuntos(nu_corrida){
    var url = 'Puntos';
    var formData = new FormData();

    formData.append("nu_corrida", nu_corrida);
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);
    if(infoData.intState){
        if(infoData.puntos !== null){
            var strHTML="";
            var chkEstatus="";
            var estatus="";

            $.each(infoData.puntos,function(index, el) {

                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" class="i-checks chkEstatusPunto" data-nu_punto="'+el.nu_punto+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                strHTML+="<tr>";
                strHTML+="<td class='text-center'>"+(index + 1)+"</td>";
                strHTML+="<td>"+el.ln_nombre+"</td>";
                strHTML+="<td class='text-center'>"+(el.nu_tipo == '0' ? 'SALIDA' : 'LLEGADA')+"</td>";
                strHTML+="<td class='text-center'>"+estatus+"</td>";
                strHTML+="</tr>";
            });

            $("#tblPuntos tbody").empty();
            $("#tblPuntos tbody").append(strHTML);

            $('#tblPuntos .i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    } 
}

function fnEditarRegistro(nu_programa){
    var url = 'Programas/'+nu_programa;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.programa !== null){
            $.each(infoData.programa,function(index, el) {

                $("#nu_programa").val(el.nu_programa);
                $("#nu_corrida").val(el.nu_corrida);
                $("#dt_fecha_inicio").val(el.dt_fecha);
                $("#dt_fecha_fin").val(el.dt_fecha);
                $("#nu_vehiculo").val(el.nu_vehiculo);
                $("#nu_conductor").val(el.nu_conductor);

                $("#dt_fecha_inicio, #dt_fecha_fin").prop('disabled', true);
                $('#mdlFormulario .selectFormatoGeneral').multiselect('rebuild');
                
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'fnCrearPrograma';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerProgramacion();
    }
}

function fnEliminarRegistro(nu_programa){
    var formData = new FormData();
    var url = 'Programas/'+nu_programa;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerProgramacion();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_programa){
    var url = 'Programas/'+nu_programa;
    var formData = new FormData(document.getElementById("frmFormulario"));
    formData.append("nu_corrida", $('#nu_corrida').val().toString());

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerProgramacion();
    }
}

function fnModificarEstatus(nu_programa,nu_activo){
    var url = 'Programas/'+nu_programa;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerProgramacion(){
    var formData = new FormData();

    formData.append("dt_fecha_inicio", $('#dtpFechaInicio').val());
    formData.append("dt_fecha_fin", $('#dtpFechaFin').val());
    formData.append("nu_origen", $('#cmbOrigen').val());
    formData.append("nu_llegada", $('#cmbDestino').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','Programas',formData);

    if(infoData.intState){
        if(infoData.programas !== null){
            var estatus="";
            var btnEliminar="";
            var btnModificar="";
            var chkEstatus="";
            var btnPuntos="";
            var jsonFechas = {};

            if($('#tblTablaGeneral').DataTable().data().length > 0){
                $('#tblTablaGeneral').DataTable().clear();
                $('#tblTablaGeneral').DataTable().draw();
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            
            $('#calendar').fullCalendar( 'destroy' );
            fnCargarCalendario({});

            $.each(infoData.programas,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_programa="'+el.nu_programa+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_programa="'+el.nu_programa+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_programa="'+el.nu_programa+'"><i class="fa fa-trash"></i></button>';
                btnPuntos='<button class="btn btn-success btn-xs btnPuntosSalida" type="button" data-toggle="modal" data-target="#mdlPuntosReunion" data-corrida="'+el.nombre_origen+" - "+el.nombre_llegada+'" data-nu_corrida="'+el.nu_corrida+'"><i class="fa fa-map-marker"></i></button>';

                $('#tblTablaGeneral').dataTable().fnAddData( 
                    [
                        (index+1),
                        el.nombre_origen + " - " + el.nombre_llegada, 
                        el.dt_fecha, 
                        el.ln_hora_salida, 
                        btnPuntos, 
                        el.nu_asiento, 
                        number_format(el.dbl_precio,2,".",","), 
                        number_format(parseFloat(el.dbl_precio) * parseFloat(el.nu_asiento),2,".",","), 
                        el.nombre_conductor +" "+el.ln_apellidos, 
                        el.ln_placa,  
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );

                $('#calendar').fullCalendar('renderEvent', {'title':el.nombre_origen + " - " + el.nombre_llegada,'start':new Date(el.dt_fecha+" "+el.ln_hora_salida),'allDay':false});

            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}
