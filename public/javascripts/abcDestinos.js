$(document).ready(function () {

    $('#mdlDestinos').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Destinos'},
            {extend: 'pdf', title: 'Destinos'},
            {extend: 'print',
                customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-center', targets: [1, 2, 3] },
        ],
    });

    fnObtenerDestinos();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_destino").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerDestinos();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_destino')>0){
            title="Modificar Destino";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_destino'));
        }else{
            title="Nuevo Destino";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_destino").val("");
            $("#ln_nombre").val("");
            $("#ln_codigo").val("");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });

});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_destino'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_destino = $(this).data('nu_destino');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_destino);
    });
    
});

function fnEditarRegistro(nu_destino){
    var url = 'Destinos/'+nu_destino;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.destino !== null){
            $.each(infoData.destino,function(index, el) {
                $("#nu_destino").val(el.nu_destino);
                $("#ln_nombre").val(el.ln_nombre);
                $("#ln_codigo").val(el.ln_codigo);
                
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'Destinos';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerDestinos();
    }
}

function fnEliminarRegistro(nu_destino){
    var formData = new FormData();
    var url = 'Destinos/'+nu_destino;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerDestinos();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_destino){
    var url = 'Destinos/'+nu_destino;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerDestinos();
    }
}

function fnModificarEstatus(nu_destino,nu_activo){
    var url = 'Destinos/'+nu_destino;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerDestinos(){
    var formData = new FormData();
    formData.append("ln_nombre", $('#txtDestino').val());
    formData.append("ln_codigo", $('#txtCodigo').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','Destinos',formData);

    if(infoData.intState){
        if(infoData.destinos !== null){
            var estatus="";
            var btnEliminar="";
            var btnModificar="";
            var chkEstatus="";

            if($('#tblDestinos').DataTable().data().length > 0){
                $('#tblDestinos').DataTable().clear();
                $('#tblDestinos').DataTable().draw();
            }

            $.each(infoData.destinos,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_destino="'+el.nu_destino+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_destino="'+el.nu_destino+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_destino="'+el.nu_destino+'"><i class="fa fa-trash"></i></button>';

                $('#tblDestinos').dataTable().fnAddData( 
                    [
                        el.ln_nombre, 
                        el.ln_codigo, 
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}
