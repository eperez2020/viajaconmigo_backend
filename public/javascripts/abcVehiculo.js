$(document).ready(function () {

    $('#mdlVehiculos').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Vehiculos'},
            {extend: 'pdf', title: 'Vehiculos'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-right', targets: [5] },
            { className: 'text-center', targets: [1,2,6,7] },
        ],
    });

    fnObtenerVehiculos();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_vehiculo").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerVehiculos();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_vehiculo')>0){
            title="Modificar Vehículo";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_vehiculo'));
        }else{
            title="Nuevo Vehículo";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_vehiculo").val("");
            $('#nu_vin').val('');
            $('#ln_placa').val('');
            $('#ln_marca').val('');
            $('#ln_modelo').val('');
            $('#nu_anio').val('');
            $('#nu_asiento').val('');
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_vehiculo'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_vehiculo = $(this).data('nu_vehiculo');
    swal({
        title: "¿Desea eliminar el vehículo?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_vehiculo);
    });
    
});

function fnEditarRegistro(nu_vehiculo){
    var url = 'Vehiculos/'+nu_vehiculo;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.vehiculo !== null){
            $.each(infoData.vehiculo,function(index, el) {
                $("#nu_vehiculo").val(el.nu_vehiculo);
                $('#nu_vin').val(el.nu_vin);
                $('#ln_placa').val(el.ln_placa);
                $('#ln_marca').val(el.ln_marca);
                $('#ln_modelo').val(el.ln_modelo);
                $('#nu_anio').val(el.nu_anio);
                $('#nu_asiento').val(el.nu_asiento);
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'Vehiculos';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerVehiculos();
    }
}

function fnEliminarRegistro(nu_vehiculo){
    var formData = new FormData();
    var url = 'Vehiculos/'+nu_vehiculo;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerVehiculos();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_vehiculo){
    var url = 'Vehiculos/'+nu_vehiculo;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerVehiculos();
    }
}

function fnModificarEstatus(nu_vehiculo,nu_activo){
    var url = 'Vehiculos/'+nu_vehiculo;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerVehiculos(){
    var formData = new FormData();
    formData.append("nu_vin", $('#txtNumeroVIN').val());
    formData.append("ln_marca", $('#txtMarca').val());
    formData.append("ln_placa", $('#txtPlaca').val());
    formData.append("nu_anio", $('#txtAnio').val());
    formData.append("ln_modelo", $('#txtModelo').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','Vehiculos',formData);

    if(infoData.intState){
        if(infoData.conductores !== null){
            var estatus="";

            var btnEliminar="";
            var btnModificar="";

            var chkEstatus="";

            if($('#tblTablaGeneral').DataTable().data().length > 0){
                $('#tblTablaGeneral').DataTable().clear();
                $('#tblTablaGeneral').DataTable().draw();
            }

            $.each(infoData.vehiculos,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_vehiculo="'+el.nu_vehiculo+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_vehiculo="'+el.nu_vehiculo+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_vehiculo="'+el.nu_vehiculo+'"><i class="fa fa-trash"></i></button>';

                $('#tblTablaGeneral').dataTable().fnAddData( 
                    [
                        el.nu_vin,
                        el.ln_placa,
                        el.nu_anio,
                        el.ln_marca,
                        el.ln_modelo,
                        el.nu_asiento,
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}