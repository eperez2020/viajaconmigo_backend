@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Programación</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmBuscar">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Inicio</label> 
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="dtpFechaInicio" id="dtpFechaInicio" type="text" class="form-control" value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha Fin</label> 
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="dtpFechaFin" id="dtpFechaFin" type="text" class="form-control" value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cmbOrigen">Origen</label>
                                        <select id="cmbOrigen" name="cmbOrigen" class="form-control selectCorridaOrigen selectFormatoGeneral">
                                            <option value="" selected="true">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cmbDestino">Destino</label>
                                        <select id="cmbDestino" name="cmbDestino" class="form-control selectCorridaDestion selectFormatoGeneral">
                                            <option value="" selected="true">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                    <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#mdlFormulario"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Programación</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblTablaGeneral" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Corrida</th>
                                        <th>Fecha</th>
                                        <th>Horario</th>
                                        <th>Punto Reunión</th>
                                        <th>Asientos</th>
                                        <th>Precio</th>
                                        <th>Total</th>
                                        <th>Conductor</th>
                                        <th>Vehículo</th>
                                        <th>Estatus</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- CALENDARIO -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Programación Vista Calendario </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal para Agregar y Modifiar  una Corrida -->
    <div class="modal inmodal" id="mdlFormulario" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-calendar modal-icon"></i>
                    <h4 class="modal-title">Programar Salida</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmFormulario" action="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputFile">Numero corrida</label>
                                    <input type="hidden" id="nu_programa" name="nu_programa" placeholder="nu_programa" class="form-control" readonly="true" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ln_hora_llegada">Corrida</label>
                                    <select id="nu_corrida" name="nu_corrida[]" class="form-control selectCorridas selectFormatoGeneral" multiple="true">
                                        <option value="" selected="true">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="data_5">
                                    <label for="ln_hora_llegada">Fecha</label>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="form-control" name="dt_fecha_inicio" id="dt_fecha_inicio" value="{{ date('Y-m-d') }}"/>
                                        <span class="input-group-addon">Hasta</span>
                                        <input type="text" class="form-control" name="dt_fecha_fin" id="dt_fecha_fin" value="{{ date('Y-m-d') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_hora_llegada">Vehículo</label>
                                    <select id="nu_vehiculo" name="nu_vehiculo" class="form-control selectVehiculos selectFormatoGeneral">
                                        <option value="" selected="true">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_hora_llegada">Conductor</label>
                                    <select id="nu_conductor" name="nu_conductor" class="form-control selectConductores selectFormatoGeneral">
                                        <option value="" selected="true">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_semanas">Repetir</label>
                                    <select id="nu_semanas" name="nu_semanas" class="form-control  selectFormatoGeneral">
                                        <option value="1" selected="true">1 Semana</option>
                                        <option value="2" >2 Semana</option>
                                        <option value="3" >3 Semana</option>
                                        <option value="4" >4 Semana</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_activo">Activo</label><br>
                                    <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                </div>
                            </div>
                        </div>   
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button id="btnAgregarNuevo" type="button" class="btn btn-primary" style="display:none;">Agregar</button>
                    <button id="btnModificarRegistro" type="button" class="btn btn-primary" style="display:none;">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- / -->

    <!-- Modal para Agregar y Modifiar  los Puntos de Reunion -->
    <div class="modal inmodal" id="mdlPuntosReunion" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <!-- <i class="fa fa-list-ol modal-icon"></i> -->
                    <h4 class="modal-title">Puntos de Reunión</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed" id="tblPuntos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th>Tipo</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- / -->

    <script src="{{asset('javascripts/programas.js')}}"></script>

@endsection