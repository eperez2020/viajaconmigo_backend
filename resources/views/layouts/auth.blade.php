<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Viaja Conmigo</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>

    </style>
</head>
<body class="gray-bg login" >

    <div class="container-fluid">
        <div class="row">
            <div class="hidden-xs col-sm-7 col-md-8">
                <div class="clearfix">
                    <div class="col-sm-12 col-md-10 col-md-offset-2">
                        <div class="logo-title-container">
                                <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{asset('img/logo_fivdevsystem_svg.svg')}}" alt="Logo Icon">
                                <div class="copy animated fadeIn">
                            </div>
                        </div> <!-- .logo-title-container -->
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

            <div class="login-container">

                @yield('content')

                <div style="clear:both"></div>

              
            </div> <!-- .login-container -->

        </div>
        </div>
    </div>

</body>
</html>
