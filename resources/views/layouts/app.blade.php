<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Viaja Conmigo</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <!-- Fonts -->
        <!-- <link href="font-awesome/css/font-awesome.css" rel="stylesheet"> -->
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Toastr style -->
        <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        <!-- Gritter -->
        <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
        
        <!-- Checkbox -->
        <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">

        <!-- Multiselect -->
        <link href="{{ asset('js/bootstrap-multiselect/bootstrap-multiselect.css') }}" rel="stylesheet">

        <!-- Sweet Alert -->
        <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

        <!-- Clockpicker -->
        <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">

        <!-- Estilos Generales -->
        <link href="{{ asset('css/estilosGenerales.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

        <link href="{{ asset('css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/fullcalendar/fullcalendar.print.css') }}" rel='stylesheet' media='print'>

        <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
        
        <link rel="shortcut icon" type="image/svg" href="{{asset('img/icon-viaja-conmigo.svg')}}"/>

        @php ini_set('max_execution_time', '300'); @endphp
    </head>
    <body>
        <div id="app">
            <div id="wrapper">
                @include('layouts.siderbar')
                <div id="page-wrapper" class="gray-bg dashbard-1">
                    @include('layouts.header')
                    @yield('content')
                    <br><br>
                    @include('layouts.footer')
                </div>
                 <!-- Scripts -->
                @include('includes.js')
            </div>
        </div>
        <!-- Cerrar sesión -->
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </body>
</html>
