<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element" align="center"> 
                    <span>
                        <img alt="image" style="width: 30%;" src="{{ asset('img/icon-viaja-conmigo.svg') }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> 
                            <span class="text-muted text-xs block">{{ Auth::user()->email }} <b class="caret"></b></span> 
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <!-- <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li> -->
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img alt="image" style="width: 40%;" src="{{ asset('img/icon-viaja-conmigo.svg') }}" />
                </div>
            </li>
            <li class="" id="mdlDashboard">
                <a href="/index" style="font-size: 14px;"><i class="fa fa-line-chart"></i> <span class="nav-label">Dashboard</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlVehiculos">
                <a href="/abcVehiculos" style="font-size: 14px;"><i class="fa fa-bus"></i> <span class="nav-label">Vehículos</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlConductores">
                <a href="/abcConductores" style="font-size: 14px;"><i class="fa fa-users"></i> <span class="nav-label">Conductores</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlDestinos">
                <a href="/abcDestinos" style="font-size: 14px;"><i class="fa fa-paper-plane"></i> <span class="nav-label">Destinos</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlCorridas">
                <a href="/abcCorridas" style="font-size: 14px;"><i class="fa fa-list-ol"></i> <span class="nav-label">Corridas</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlProgramacion">
                <a href="/Programacion" style="font-size: 14px;"><i class="fa fa-calendar"></i> <span class="nav-label">Programación</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlUsuarios">
                <a href="/abcUsuarios" style="font-size: 14px;"><i class="fa fa-user"></i> <span class="nav-label">Usuarios</span> <!-- <span class="fa arrow"></span> --></a>
            </li>       
        </ul>
    </div>
</nav>