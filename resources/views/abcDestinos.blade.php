@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Destinos</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmFiltros">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" id="data_1">
                                        <label>Destino</label> 
                                        <input name="txtDestino"  type="text" id="txtDestino" placeholder="Nombre Destino" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="data_1">
                                        <label>Código</label> 
                                        <input name="txtCodigo"  type="text" id="txtCodigo" placeholder="Código" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#mdlFormulario"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Destinos</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblDestinos" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Destino</th>
                                        <th>Código</th>
                                        <th>Estatus</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="mdlFormulario" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-paper-plane modal-icon"></i>
                    <h4 class="modal-title">Destinos</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmFormulario" action="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="nu_destino">Numero destino</label>
                                    <input type="hidden" id="nu_destino" name="nu_destino" placeholder="nu_destino" class="form-control" readonly="true" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_nombre">Nombre Destino</label>
                                    <input type="text" id="ln_nombre" name="ln_nombre" placeholder="Nombre Destino" class="form-control" maxlength="150">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_codigo">Código</label>
                                    <input type="text" id="ln_codigo" name="ln_codigo" placeholder="Código" class="form-control" maxlength="20">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_activo">Activo</label><br>
                                    <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button id="btnAgregarNuevo" type="button" class="btn btn-primary" style="display:none;">Agregar</button>
                    <button id="btnModificarRegistro" type="button" class="btn btn-primary" style="display:none;">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('javascripts/abcDestinos.js') }}"></script>

@endsection