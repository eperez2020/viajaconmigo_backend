@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <!-- <div class="title m-b-md">
    <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAEpUlEQVR4nO3dwY7kJhSG0cwo7//Io2wrC08g8BmX55xlqxpTrV/0Fb7GP379+vUX7Pbz9AR4J8EiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWib/vuczPn/sTfHUW4ee1Vs4rvJrz55hX15r9viPfZZd7znC0YpEQLBKCReKmGutTUfeM1CIjNdPIz2d/d6VWm53PlaJW+48r3nw9/hCCRUKwSByosT6N/O+frXU+x5ytq0b2pVZqnV1j7vq7daxYJASLhGCROFxj7TK7V3RlpC6ZrY1G5rCrhnsOKxYJwSIhWCReUmOt7EvNjr+yT1bsjT2TFYuEYJEQLBKHa6xdtUWxt7Qyfj3O82syKxYJwSIhWCQO1Fj391//fg4rzwnu6t8a+fwT/m7jvmmufBHBIiFYJH48f0eksOtZv5U+sHf/5a1YJASLhGCR+ILzsVb6qK7GWRl/ZO9qZR9rZG4rP7+HFYuEYJEQLBIH9rFW+tDvrC2Kem7XWRJXnnOmgxWLhGCRECwSD71XuFIrzNZwK2dT3XnW/Mp9TPtYvIRgkRAsEg/tea/7xO88g7To69p1/7TzxDnxAoJFQrBIHN7H2tUzvquHfcQT7kV+so/FH0SwSAgWiS84H2ulhqjf2Xx1rdlxVu5pXn3Hs/3vViwSgkVCsEg89LnClXpopXd+RHG+w8jPZ9nH4oUEi4RgkTiwj7XSY1S8+3n281d7RSPjz37mysh+1VlPmQcvI1gkBIvEg54rrM/Q2nXuQ937teu5yLNnn1qxSAgWCcEicfh8rE9F/3j97XbdlyzOx9KPxQsJFgnBInFTjVW8u2Zlf+hqDrOfL+Zc9K7px+IlBIuEYJG4qR9rV/1R95Wv3K/c9btPu3f5/1ixSAgWCcEicfi5wpVap6gVZses65iiFrzH0+fHlxIsEoJF4qH3Cq8+MzLmlfrd0sW7embH8b5CXk6wSAgWiQM11q73QI98fnZus2Pe+X7rlbmNfH4vKxYJwSIhWCQedM77bE9S3f9e3I8r7iGe+i6/Z8UiIVgkBIvE4Rpr1q6zDD7N1iKzY87aNYezPVtWLBKCRUKwSDzoDNIVp/qQdr33sDjf4fDbvg9emxcTLBKCReKh7ysccef5WMUZELv2q4rfXWfFIiFYJASLxOH3Fc7a9cxdccbp1XyKZypn53A/KxYJwSIhWCQO92Ptuue18lzeyLXuPPO9PlfCc4V8McEiIVgkvqznfcSuOmaln31lX+rOmrLzlHnwMoJFQrBIvLDG+nRVi4zUJbM/39UvNTL+2ffkjLBikRAsEoJF4kHnY91pV4/UbB12Nc7KZ66uqx+LFxIsEoJF4kCNdef9rNk6ZqXvauUe5cqYz+nB+tfVb74efwjBIiFYJF5yPhZPY8UiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEi8Q842PI7jcm/CQAAAABJRU5ErkJggg==">
    </div> -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Conductores</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmFiltros" >
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" id="data_1">
                                        <label>Nombre</label> 
                                        <input name="txtNombre"  type="text" id="txtNombre" placeholder="Nombre Conductor" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#mdlFormulario"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Conductores</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblConductores" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Conductor</th>
                                        <th>Dirección</th>
                                        <th>Telefono</th>
                                        <th>Edad</th>
                                        <th>Estatus</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="mdlFormulario" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-users modal-icon"></i>
                    <h4 class="modal-title">Conductor</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmFormulario" action="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputFile">Numero conducto</label>
                                    <input type="hidden" id="nu_conductor" name="nu_conductor" placeholder="nu_conductor" class="form-control" readonly="true" >

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Nombre Conductor</label>
                                    <input type="text" id="ln_nombre" name="ln_nombre" placeholder="Nombre Conductor" class="form-control" maxlength="40">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Apellidos Conductor</label>
                                    <input type="text" id="ln_apellidos" name="ln_apellidos" placeholder="Apellidos Conductor" class="form-control" maxlength="70">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="dtpFechaNacimiento">
                                    <label for="exampleInputFile">Fecha Nacimiento</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="dt_fecha_nacimiento" id="dt_fecha_nacimiento" type="text" class="form-control" value="{{ date('Y-m-d') }}">
                                    </div>
                                    <!-- <input type="text" id="dt_fecha_nacimiento" name="dt_fecha_nacimiento" placeholder="Fecha Nacimiento" class="form-control" > -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputFile">Dirección</label>
                                    <input type="text" id="ln_direccion" name="ln_direccion" placeholder="Dirección" class="form-control" maxlength="300">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Teléfono</label>
                                    <input type="text" id="ln_telefono" name="ln_telefono" placeholder="Teléfono" class="form-control" maxlength="25" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_activo">Activo</label><br>
                                    <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button id="btnAgregarNuevo" type="button" class="btn btn-primary" style="display:none;">Agregar</button>
                    <button id="btnModificarRegistro" type="button" class="btn btn-primary" style="display:none;">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('javascripts/abcConductores.js') }}"></script>

@endsection