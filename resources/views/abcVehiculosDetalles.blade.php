@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Vehículo</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Vehículo</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmVehiculo">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" id="data_1">
                                        <label>Número VIN</label> 
                                        <input name="txtNumeroVIN"  type="text" id="txtNumeroVIN" placeholder="Número VIN" class="form-control" maxlength="19">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="data_2">
                                        <label>Placa</label> 
                                        <input name="txtPlaca"  type="text" id="txtPlaca" placeholder="Placa" class="form-control inputMayusculas" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Año</label> 
                                        <input name="txtAnio" type="text" id="txtAnio" placeholder="Año" class="form-control soloNumeros" maxlength="4">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Modelo</label> 
                                        <input name="txtModelo"  type="text" id="txtModelo" placeholder="Modelo" class="form-control" maxlength="255">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Marca</label> 
                                        <input name="txtMarca"  type="text" id="txtMarca" placeholder="Marca" class="form-control" maxlength="255">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Asientos</label> 
                                        <input name="txtAsientos"  type="text" id="txtAsientos" placeholder="Asientos" class="form-control soloNumeros" maxlength="2">
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <a href="{{ url('abcVehiculos') }}"  class="btn btn-success btn-sm" ><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<span class="bold">Regresar</span></a>
                        <button class="btn btn-success btn-sm" type="button"><i class="fa fa-save"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button>
                        <!-- <button class="btn btn-primary dim " type="button" id="btnBuscarOrden"><i class="fa fa-search"></i> Buscar</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <script src="{{ asset('javascripts/abcVehiculo.js') }}"></script> -->

@endsection