<!-- Mainly scripts -->
<!-- <script src="{{ asset('js/jquery-2.1.1.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script> 

<!-- Flot -->
<!-- <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script> -->

<!-- Peity -->
<!-- <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/demo/peity-demo.js') }}"></script> -->

<!-- Custom and plugin javascript -->
<script src="{{ asset('js/inspinia.js') }}"></script>
<!-- <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script> -->

<!-- jQuery UI -->
<!-- <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script> -->

<!-- GITTER -->
<!-- <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script> -->

<!-- Sparkline -->
<script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<!-- <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script> -->

<!-- Toastr -->
<script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>

<!-- Table -->
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Multiselect -->
<script src="{{ asset('js/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Clock picker -->
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>



<!-- Full Calendar -->
<script src="{{ asset('js/plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script> 

<!-- Archivo para funciones generales de Javascript -->
<!-- <script src="{{ asset('javascripts/funcionesGenerales.js') }}"></script> -->

<!-- Mainly scripts -->

<script type="text/javascript">
    $(document).ready(function() {
        
        /* MENSAJE DE BIENVENIDO */
        if (window.location.pathname == "/home") {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Bienvenido!', '');
            }, 1300);
        }

        /* FORMATO PARA LOS CHECKS */
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        /* FORMATOS PARA INPUT FECHA */
        $('.input-group.date, .input-daterange').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });

        /* VALIDACION INPUT SOLO NUMEROS */
        $(".soloNumeros").keydown(function(event) {
            // Desactivamos cualquier combinación con shift
            if(event.shiftKey)
            event.preventDefault();
            
            if ( $( this ).hasClass( "porcentaje" ) ) {
                // Si tiene porcentaje, validar
                var chark = String.fromCharCode(event.keyCode);
                var tempValue = $(this).val()+chark;
                if (Number(tempValue) > Number(100)) {
                    // Es mayor a 100
                    return false;
                }
            }

            /*  
            No permite ingresar pulsaciones a menos que sean los siguientes
            KeyCode Permitidos
            keycode 8 Retroceso
            keycode 37 Flecha Derecha
            keycode 39  Flecha Izquierda
            keycode 46 Suprimir
            */
            //No permite mas de 11 caracteres Numéricos
            //
            if (event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39) 
            if($(this).val().length >= 11)
            event.preventDefault();

            // Solo Numeros del 0 a 9 
            if (event.keyCode < 48 || event.keyCode > 57)
            //Solo Teclado Numerico 0 a 9
            if (event.keyCode < 96 || event.keyCode > 105)
            
            /*  
            No permite ingresar pulsaciones a menos que sean los siguietes
            KeyCode Permitidos
            keycode 8 Retroceso
            keycode 37 Flecha Derecha
            keycode 39  Flecha Izquierda
            keycode 46 Suprimir
            */
           //
            if( event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39)
            event.preventDefault();
        });

        $(".inputMayusculas").keyup(function(event){
            return this.value=this.value.toUpperCase();
        });

        $('.clockpicker').clockpicker();
    });

    $(document).on("click",".paginate_button",function() {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    function fnAjaxSelect(tipo, ruta, params){
        var url = window.location.origin+"/"+ruta;
        var infoData = [];

        $.ajax({
            async:false,
            url:  url,
            type: tipo,
            dataType: "json",
            data: params,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(data){
            if(data.intState){
                infoData =  data;
            }else{
                swal({
                    title: "Conexión",
                    text: data.strMensaje,
                    type: "error"
                });
                infoData =  data;
            }
        });

        return infoData;
    }
    
    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    /* MENSAJE SUCCESS INFORMATIVO LATERAL TIPO TOASTR */
    function fnMensajeInformativoSuccess(strMensaje, timeOut=4000, timeIn=10){
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: timeOut
            };
            toastr.success(strMensaje, '');
        }, timeIn);
    }

    function fnFormatoSelectGeneral(idClase) {
        $(''+idClase).multiselect({
            enableFiltering: true,
            filterBehavior: 'text',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            numberDisplayed: 1,
            includeSelectAllOption: true
        });

        // Estilos
        $('.multiselect-container').css({
            'max-height': "200px"
        });
        $('.multiselect-container').css({
            'overflow-y': "scroll"
        });
    }

    function fnListadoSelectGeneral(elemento, dataJson, limpiar = 0) {
        var listado = "";

        if ($(""+elemento).prop("multiple")) {
            $(""+elemento).empty();
        }

        if (limpiar == 1) {
            $(""+elemento).empty();
            if (!$(""+elemento).prop("multiple")) {
                listado += "<option value=''>Seleccionar</option>";
            }
        }

        var valorSelect = "";
        if ($(""+elemento).prop("value")) {
            valorSelect = $(""+elemento).prop("value");
        }

        var valor = "";
        var texto = "";
        var contador = 1;
        var selected = '';

        for (var info in dataJson) {
            contador = 1;

            for (var info2 in dataJson[info]) {
                if (contador == 1) {
                    valor = dataJson[info][info2];
                } else {
                    texto = dataJson[info][info2];
                }
                contador ++;
            }

            selected = '';
            if (valorSelect == valor) {
                selected = 'selected="true"';
            }

            listado += "<option value='"+valor+"' "+selected+">"+texto+"</option>";
        }

        $(""+elemento).append(listado);

        fnFormatoSelectGeneral(""+elemento);

        if (limpiar == 1) {
            $(''+elemento).multiselect('rebuild');
        }
    }

    if (document.querySelector(".selectCorridaOrigen, .selectCorridaDestion")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnCargarComboDestinos',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectCorridaOrigen', infoData.datos, 1);
                fnListadoSelectGeneral('.selectCorridaDestion', infoData.datos, 1);
            }
        }
    }

    if (document.querySelector(".selectVehiculos")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnCargarComboVehiculos',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectVehiculos', infoData.datos, 1);
            }
        }
    }
    
    if (document.querySelector(".selectConductores")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnCargarComboConductores',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectConductores', infoData.datos, 1);
            }
        }
    }

    if (document.querySelector(".selectCorridas")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnCargarComboCorridas',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectCorridas', infoData.datos, 1);
            }
        }
    }
</script>