@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Corridas</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmBuscarOrden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cmbOrigen">Origen</label>
                                        <select id="cmbOrigen" name="cmbOrigen" class="form-control selectCorridaOrigen selectFormatoGeneral">
                                            <option value="" selected="true">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cmbDestino">Destino</label>
                                        <select id="cmbDestino" name="cmbDestino" class="form-control selectCorridaDestion selectFormatoGeneral">
                                            <option value="" selected="true">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#mdlFormulario"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Corridas</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblTablaGeneral" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Hora Salida</th>
                                        <th>Hora Llegada</th>
                                        <th>Puntos Reunión</th>
                                        <th>Precio</th>
                                        <th>Estatus</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal para Agregar y Modifiar  una Corrida -->
    <div class="modal inmodal" id="mdlFormulario" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-list-ol modal-icon"></i>
                    <h4 class="modal-title">Corridas</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmFormulario" action="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputFile">Numero corrida</label>
                                    <input type="hidden" id="nu_corrida" name="nu_corrida" placeholder="nu_corrida" class="form-control" readonly="true" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Origen</label>
                                    <select id="nu_origen" name="nu_origen" class="form-control selectCorridaOrigen selectFormatoGeneral">
                                        <option value="" selected="true">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Destino</label>
                                    <select id="nu_llegada" name="nu_llegada" class="form-control selectCorridaDestion selectFormatoGeneral">
                                        <option value="" selected="true">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="dbl_precio">Precio</label>   
                                    <input name="dbl_precio"  type="text" id="dbl_precio" placeholder="Precio" class="form-control ">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputFile">Hora Salida</label>
                                    <div class="input-group clockpicker" data-autoclose="true">
                                        <input name = "ln_hora_salida" id = "ln_hora_salida" type="text" class="form-control" value="07:30" >
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_hora_llegada">Hora Llegada</label>
                                    <div class="input-group clockpicker" data-autoclose="true">
                                        <input name = "ln_hora_llegada" id = "ln_hora_llegada" type="text" class="form-control" value="09:30" >
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_activo">Activo</label><br>
                                    <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button id="btnAgregarNuevo" type="button" class="btn btn-primary" style="display:none;">Agregar</button>
                    <button id="btnModificarRegistro" type="button" class="btn btn-primary" style="display:none;">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- / -->

    <!-- Modal para Agregar y Modifiar  los Puntos de Reunion -->
    <div class="modal inmodal" id="mdlPuntosReunion" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <!-- <i class="fa fa-list-ol modal-icon"></i> -->
                    <h4 class="modal-title">Puntos de Reunión</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmPuntosReunion" action="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputFile">Numero corrida</label>
                                    <input type="hidden" id="nu_corrida2" name="nu_corrida2" placeholder="Corrida" class="form-control" readonly="true" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Corrida">Corrida</label>   
                                    <input id="txtDescripcionCorrida" type="text" placeholder="Corrida" class="form-control" readonly = "true">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_nombre">Descripción</label>   
                                    <input name="ln_nombre"  type="text" id="ln_nombre" placeholder="Descripción" class="form-control ">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nu_tipo">Tipo</label>
                                    <select id="nu_tipo" name="nu_tipo" class="form-control selectFormatoGeneral">
                                        <option value="0" selected="true">Salida</option>
                                        <option value="1" >Llegada</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btnAgregarPunto" type="button" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed" id="tblPuntos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th>Tipo</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- / -->

    <script src="{{ asset('javascripts/abcCorridas.js') }}"></script>

@endsection