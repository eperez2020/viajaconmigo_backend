@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">
                    <img alt="image" style="width: 70%;" src="{{ asset('img/icon-viaja-conmigo.svg') }}" />
                </h1>
            </div>
            <!-- <h3>Bienvenido a "El Software de Hoy"</h3> -->
            <p><!-- Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views. -->
            </p>
            <br>
            <br>
            <p class="leyendaIniciarSesion ">Iniciar sesión aquí</p>
            <br>
            <form class="m-t" role="form" action="index.html">
                <div class="form-group grouplogin">
                    <input id="email" type="email" class=" inputLogin form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group grouplogin">
                    <input id="password" type="password" class="inputLogin form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="form-check pull-left remember-me-text grouplogin">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember" style="font-weight: normal;">
                        {{ __('Recuérdame') }}
                    </label>
                </div>
                <br>                 
                <br> 
                <br>            
                <button type="submit" class="btn btn-success btnLogin block full-width m-b grouplogin">Iniciar Sesión</button>

                <!-- <a href="#"><small>Forgot password?</small></a> -->
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p> -->
                <!-- <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            </form>
            <p class="m-t"> 
                <small>Copyright <strong>FivDevSystem</strong> © 2020-2021</small>
            </p>
        </div>
    </div>
</form>
@endsection
