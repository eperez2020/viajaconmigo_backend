<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corridas extends Model
{
    protected $fillable = ['nu_origen','nu_llegada','ln_hora_salida','ln_hora_llegada','dbl_precio','nu_activo'];
    protected $primaryKey = 'nu_corrida';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeNuOrigen($query, $nu_origen = '') {
        if (!empty($nu_origen and !is_null($nu_origen))) {
    	    return $query->where('nu_origen', $nu_origen);
        }
    }
    
    public function scopeNuDestino($query, $nu_llegada = '') {
        if (!empty($nu_llegada) and !is_null($nu_llegada)) {
    	    return $query->where('nu_llegada', $nu_llegada);
        }
    }

    public function scopeNuActivo($query, $nu_activo = '') {
        if (!empty($nu_activo) and !is_null($nu_activo)) {
    	    return $query->where('nu_activo', $nu_activo);
        }
    }

    public function Destinos() {
        return $this->hasMany(Destinos::class, 'nu_llegada', 'nu_origen');
    }
}
