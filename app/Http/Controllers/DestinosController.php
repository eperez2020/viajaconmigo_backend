<?php

namespace App\Http\Controllers;

use App\Destinos;
use App\Corridas;
use Illuminate\Http\Request;

class DestinosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $destinos = Destinos::LnNombre($dataRequest['ln_nombre'])
                                ->LnCodigo($dataRequest['ln_codigo'])
                                ->orderBy('ln_nombre', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los destinos correctamente.","destinos"=>$destinos],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","destinos"=>""],400) ;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            
            $estatus=0;

            $destino = Destinos::LnNombre($request->ln_nombre)->first();

            if($destino){
                return response()->json(["intState"=>2,"strMensaje"=>"Ya existe el destino: ".$request->ln_nombre,"cliente"=>""],400) ;
            }

            if(isset($request->nu_activo)){
                $estatus=1;
            }

            $destino=Destinos::create([
                'ln_nombre'=>$request->ln_nombre,
                'ln_codigo'=>$request->ln_codigo,
                'nu_activo'=>$estatus
            ]);

            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el destino: ". $request->ln_nombre,"destino"=>compact('destino')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","destino"=>""],400) ;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function show(Destinos $destinos, $nu_destino)
    {
        if(request()->ajax()){
            $destino = Destinos::findOrFail($nu_destino);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","destino"=>compact("destino")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar el destino.","destino"=>""],400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function edit(Destinos $destinos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Destinos $destinos, $nu_destino)
    {
        if(request()->ajax()){

            $dataRequest = request()->all();

            $destino = Destinos::findOrFail($nu_destino);

            if (!isset($dataRequest['ln_nombre'])) {
                $dataRequest['ln_nombre'] = $destino->ln_nombre;
            }

            if (!isset($dataRequest['ln_codigo'])) {
                $dataRequest['ln_codigo'] = $destino->ln_codigo;
            }

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }
            
            $destino->update($dataRequest);

            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente: ".$dataRequest['ln_nombre'],"destino"=>compact('destino')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","destino"=>""],400);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destinos $destinos, $nu_destino)
    {
        if(request()->ajax()){
            $corridasorigen = Corridas::where('nu_origen',$nu_destino)->first();
            
            if($corridasorigen){
                return response()->json(["intState"=>0,"strMensaje"=>"Problemas el destino esta asociado a una corrida, consultar corridas."],200);
            }else{
                
                $corridasllegada = Corridas::where('nu_llegada',$nu_destino)->first();
                if($corridasllegada){
                    return response()->json(["intState"=>0,"strMensaje"=>"Problemas el destino esta asociado a una corrida, consultar corridas."],200);
                }

                $destino = Destinos::findOrFail($nu_destino);
                $destino->delete();
                return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente el vehículo: ".$destino->ln_nombre,"destino"=>$destino],200);
            }
            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","destino"=>""],400);
        }
    }

    public function fnCargarComboDestinos(){
        $destinos = Destinos::select(['nu_destino','ln_nombre'])
                            ->where('nu_activo', '1')
                            ->orderBy('ln_nombre', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$destinos],200);  
    }
}
