<?php

namespace App\Http\Controllers;

use App\Puntos;
use Illuminate\Http\Request;

class PuntosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $puntos = Puntos::NuCorrida($dataRequest['nu_corrida'])
                                ->orderBy('ln_nombre', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los puntos correctamente.","puntos"=>$puntos],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","puntos"=>""],400) ;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $estatus=0;

            if(isset($dataRequest['nu_activo'])){
                $estatus=1;
            }

            $punto=Puntos::create([
                'nu_corrida'=>$dataRequest['nu_corrida'],
                'ln_nombre'=>$dataRequest['ln_nombre'],
                'nu_tipo'=>$dataRequest['nu_tipo'],
                'nu_activo'=>$estatus
            ]);
            
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el punto","punto"=>compact('punto')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","punto"=>""],400) ;            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Puntos  $puntos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Puntos $puntos, $nu_punto)
    {
        if(request()->ajax()){
            
            $dataRequest = request()->all();

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            $punto = Puntos::findOrFail($nu_punto);

            $punto->update($dataRequest);
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente: ".$punto->ln_nombre,"punto"=>compact('punto')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","punto"=>""],400);
        } 
    }

    public function fnComboPuntosReunion(Request $request){
        $puntos_salida = Puntos::select('nu_punto AS id', 'ln_nombre AS description')
                        ->NuCorrida($request->nu_corrida)
                        ->NuActivo(1)
                        ->NuTipo('0')
                        ->orderBy('ln_nombre', 'ASC')->get();
        
        $puntos_llegada = Puntos::select('nu_punto AS id', 'ln_nombre AS description')
                        ->NuCorrida($request->nu_corrida)
                        ->NuActivo(1)
                        ->NuTipo('1')
                        ->orderBy('ln_nombre', 'ASC')->get();
        if($puntos_salida){
            return response()->json(["intState"=>1, "puntos_salida"=>$puntos_salida, "puntos_llegada"=>$puntos_llegada],200) ;
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"No se encontraron punto de reunion.","datos"=>""],400) ;
        }
    }
}
