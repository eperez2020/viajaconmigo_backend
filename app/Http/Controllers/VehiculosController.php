<?php

namespace App\Http\Controllers;

use App\Vehiculos;
use App\Programas;
use Illuminate\Http\Request;

class VehiculosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $vehiculos = Vehiculos::NuVin($dataRequest['nu_vin'])
                                ->LnPlaca($dataRequest['ln_placa'])
                                ->LnMarca($dataRequest['ln_marca'])
                                ->LnModelo($dataRequest['ln_modelo'])
                                ->NuAnio($dataRequest['nu_anio'])
                                ->orderBy('nu_vin', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los vehiculos correctamente.","vehiculos"=>$vehiculos],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","vehiculos"=>""],400) ;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $estatus=0;

            $vehiculo = Vehiculos::where('nu_vin',$dataRequest['nu_vin'])->first();
            if($vehiculo){
                return response()->json(["intState"=>2,"strMensaje"=>"Ya existe el vehículo: ".$dataRequest['nu_vin'],"cliente"=>""],400) ;
            }

            if(isset($dataRequest['nu_activo'])){
                $estatus=1;
            }

            $vehiculo=Vehiculos::create([
                'nu_vin'=>$dataRequest['nu_vin'],
                'ln_placa'=>$dataRequest['ln_placa'],
                'ln_marca'=>$dataRequest['ln_marca'],
                'ln_modelo'=>$dataRequest['ln_modelo'],
                'nu_anio'=>$dataRequest['nu_anio'],
                'nu_asiento'=>$dataRequest['nu_asiento'],
                'nu_activo'=>$estatus
            ]);
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el vehiculo: ". $dataRequest['nu_vin'],"vehiculo"=>compact('vehiculo')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","vehiculo"=>""],400) ;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehiculos  $vehiculos
     * @return \Illuminate\Http\Response
     */
    public function show(Vehiculos $vehiculos, $nu_vehiculo)
    {
        if(request()->ajax()){
            $vehiculo = Vehiculos::findOrFail($nu_vehiculo);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","vehiculo"=>compact("vehiculo")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar vehiculo.","vehiculo"=>""],400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehiculos  $vehiculos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehiculos $vehiculos, $nu_vehiculo)
    {
        if(request()->ajax()){
            //$conductores = Conductores::findOrFail($nu_conductor) ;
            $dataRequest = request()->all();

            // if (!isset($dataRequest['nu_vin'])) {
            //     $dataRequest['nu_vin'] = '';
            // }

            // if (!isset($dataRequest['ln_placa'])) {
            //     $dataRequest['ln_placa'] = '';
            // }

            // if (!isset($dataRequest['ln_marca'])) {
            //     $dataRequest['ln_marca'] = '';
            // }

            // if (!isset($dataRequest['ln_modelo'])) {
            //     $dataRequest['ln_modelo'] = '';
            // }

            // if (!isset($dataRequest['nu_anio'])) {
            //     $dataRequest['nu_anio'] = '';
            // }

            // if (!isset($dataRequest['nu_asiento'])) {
            //     $dataRequest['nu_asiento'] = '';
            // }

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            $vehiculo = Vehiculos::findOrFail($nu_vehiculo);

            $vehiculo->update($dataRequest);
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente: ".$vehiculo->nu_vin,"vehiculo"=>compact('vehiculo')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","vehiculo"=>""],400);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehiculos  $vehiculos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehiculos $vehiculos, $nu_vehiculo)
    {
        if(request()->ajax()){
            $programas = Programas::where('nu_vehiculo',$nu_vehiculo)->first();
            
            if($programas){
                return response()->json(["intState"=>0,"strMensaje"=>"Problemas el vehículo esta asociado a una salida, consultar programación."],200);
            }else{
                $vehiculo = Vehiculos::findOrFail($nu_vehiculo);
                $vehiculo->delete();
                return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente el vehículo: ".$vehiculo->nu_vin,"vehiculo"=>$vehiculo],200);    
            }

        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","vehiculo"=>""],400);
        }
    }

    public function fnCargarComboVehiculos(){
        $vehiculos = Vehiculos::select(['nu_vehiculo',"ln_placa"])
                            ->where('nu_activo', '1')
                            ->orderBy('ln_placa', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$vehiculos],200);  
    }
}
