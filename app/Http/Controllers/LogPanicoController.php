<?php

namespace App\Http\Controllers;

use App\LogPanico;
use Illuminate\Http\Request;

class LogPanicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->nu_panico)){
            $logPanico=LogPanico::create([
                'nu_panico'=>$request->nu_panico,
                'nu_cliente'=>$request->nu_cliente
            ]);
    
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el programa: ","logpanico"=>compact('logPanico')],200) ;   
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Es necesario el servicio de panico"],200) ;    
        }

                 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LogPanico  $logPanico
     * @return \Illuminate\Http\Response
     */
    public function show(LogPanico $logPanico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LogPanico  $logPanico
     * @return \Illuminate\Http\Response
     */
    public function edit(LogPanico $logPanico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LogPanico  $logPanico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LogPanico $logPanico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LogPanico  $logPanico
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogPanico $logPanico)
    {
        //
    }
}
