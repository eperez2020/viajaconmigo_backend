<?php

namespace App\Http\Controllers;

use App\Reservados;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ReservadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->nu_programa)){
            if($request->nu_programa){
                $reservados = Reservados::select(['reservados.nu_asiento','reservados.nu_estatus','reservados.ln_nombre','puntosalida.ln_nombre as punto_salida','puntollegada.ln_nombre as punto_llegada','clientes.avatar'])
                    ->join('puntos as puntosalida', 'puntosalida.nu_punto', 'reservados.nu_salida')
                    ->join('puntos as puntollegada', 'puntollegada.nu_punto', 'reservados.nu_llegada')
                    ->join('clientes', 'clientes.nu_cliente', 'reservados.nu_cliente')
                    ->where('nu_programa', $request->nu_programa)
                    ->orderBy('nu_asiento', 'ASC')->get();
                return response()->json(["intState"=>1, "reservados"=>$reservados],200);
            }else{
                return response()->json(["intState"=>0, "strMensaje"=>"Es necesario seleccioanr una salida", "reservados"=>""],200);
            }
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"Es necesario el parametro nu_programa", "reservados"=>""],200);
        }  
    }

    public function fnHistorialReservaciones(Request $request){
        if(isset($request->nu_cliente)){
            $reservacion = Reservados::select(
                'reservados.nu_reservado',
                'reservados.nu_programa',
                'reservados.nu_asiento',
                'reservados.nu_estatus',
                'reservados.nu_puntuacion',
                'reservados.ln_nombre',
                'reservados.ln_comentario',
                'reservados.ln_qr',
                'programas.dt_fecha',
                'corridas.ln_hora_salida',
                'corridas.ln_hora_llegada',
                'corridas.dbl_precio',
                'origenes.ln_nombre as nombre_origen', 
                'llegadas.ln_nombre as nombre_llegada',
                'puntosalida.ln_nombre as punto_salida',
                'puntollegada.ln_nombre as punto_llegada',
                'vehiculos.ln_placa',
                'vehiculos.ln_marca',
                'vehiculos.ln_modelo',
                'vehiculos.nu_anio',
                'conductores.ln_nombre as nombre_conductor',
                'conductores.ln_apellidos',
                'conductores.ln_telefono')
            ->join('programas', 'reservados.nu_programa', 'programas.nu_programa')
            ->join('corridas', 'corridas.nu_corrida', 'programas.nu_corrida')
            ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
            ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
            ->join('puntos as puntosalida', 'puntosalida.nu_punto', 'reservados.nu_salida')
            ->join('puntos as puntollegada', 'puntollegada.nu_punto', 'reservados.nu_llegada')
            ->join('vehiculos', 'vehiculos.nu_vehiculo', 'programas.nu_vehiculo')
            ->join('conductores', 'conductores.nu_conductor', 'programas.nu_conductor')
            ->NuCliente($request->nu_cliente)
            ->orderByDesc('reservados.nu_reservado')
            ->limit(20)->get();

            return response()->json(["intState"=>1, "reservacion"=>$reservacion],200);
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"Es necesario el parametro nu_reservado", "reservacion"=>""],200);
        }
    }

    public function fnGenerarReservacion(Request $request){
        try {
            $reservado = Reservados::where('nu_asiento',$request->nu_asiento)
                                    ->where('nu_programa', $request->nu_programa)
                                    ->where('nu_estatus','!=', '2')->first();
            if($reservado){
                return response()->json(["intState"=>0, "strMensaje"=>"El asiento ya se encuentra reservado, intente con un asiento nuevo.", "reservado"=>""],200);
            }else{
                $reservacion=Reservados::create([
                    "nu_programa" => $request->nu_programa,
                    "nu_cliente" => $request->nu_cliente,
                    "nu_asiento" => $request->nu_asiento,
                    "ln_nombre" => '',
                    "nu_puntuacion" => '0',
                    "ln_comentario" => '',
                    "nu_salida" => '0',
                    "nu_llegada" => '0',
                    "nu_estatus" => "0",
                ]);

                $qrCadena = 'Número Reservación:'.$reservacion->nu_reservado;
                $qrCadena .= ", Asiento: ".$reservacion->nu_asiento;
                $qrCadena .= ", Cliente: ".$reservacion->ln_nombre;
                $qrCode=base64_encode(QrCode::format('png')->size(200)->generate($qrCadena));

                $reservacion->update(["ln_qr" => $qrCode]);

                return response()->json(["intState"=>1, "strMensaje"=>"El viaje se registro correctamente..", "reservacion"=>$reservacion, "qrcode"=>$qrCode],200);
            }
            
        } catch (\Throwable $th) {
            return response()->json(["intState"=>0,"strError"=>$th, "strMensaje"=>"El asiento ya se encuentra reservado, intenta con uno nuevo.", "reservado"=>""],200);
        }
    }

    public function fnConfirmarReservacion(Request $request){
        $reservacion = Reservados::find($request->nu_reservado);
        if($reservacion){
            if($reservacion->nu_estatus == "1"){
                return response()->json(["intState"=>0, "strMensaje"=>"El viaje ya fue confirmado previamente.", "reservados"=>$reservacion],200);
            }

            $qrCadena = 'Número Reservación:'.$reservacion->nu_reservado;
            $qrCadena .= ", Asiento: ".$reservacion->nu_asiento;
            $qrCadena .= ", Cliente: ".$request->ln_nombre;
            $qrCode=base64_encode(QrCode::format('png')->size(200)->generate($qrCadena));

            $reservacion->update(["nu_estatus" => "1", "ln_nombre" => $request->ln_nombre, "nu_salida" => $request->nu_salida,"nu_llegada" => $request->nu_llegada, "ln_qr" => $qrCode]);
            return response()->json(["intState"=>1, "strMensaje"=>"Se confirmó correctamente el viaje.", "reservados"=>$reservacion],200);
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"No se encontro la reservacion", "reservados"=>""],200);
        }
    }

    public function fnCancelarReservacion(Request $request){
        $reservacion = Reservados::find($request->nu_reservado);
        if($reservacion){
            if($reservacion->nu_estatus == "2"){
                return response()->json(["intState"=>0, "strMensaje"=>"El viaje ya fue cancelado previamente.", "reservados"=>""],200);
            }
            $reservacion->update(["nu_estatus" => "2"]);
            return response()->json(["intState"=>1, "strMensaje"=>"Se canceló correctamente el viaje.", "reservados"=>""],200);
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"No se encontro la reservacion", "reservados"=>""],200);
        }
    }

    public function fnCalificacionReservacion(Request $request){
        $reservacion = Reservados::find($request->nu_reservado);
        if($reservacion){
            $reservacion->update(["nu_puntuacion" => $request->nu_puntuacion, "ln_comentario" => $request->ln_comentario]);
            return response()->json(["intState"=>1, "strMensaje"=>"Se calificó correctamente el viaje.", "reservados"=>""],200);
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"No se encontro la reservacion", "reservados"=>""],200);
        }
    }

    public function fnConfirmarAsistencia(Request $request){
        /* Obtener nuemero de reservacion de la cadena QR */
        $arrInfoReservacion = explode(',', $request->qrCodigo);
        $arrNumReservacion = explode(':', $arrInfoReservacion[0]);
        $nu_reservado = trim($arrNumReservacion[1]);

        $reservacion = Reservados::find($nu_reservado);
        if($reservacion){
            /* Validar si el asiento ya fue confirmado */
            if($reservacion->nu_estatus == "3"){
                return response()->json(["intState"=>0, "strMensaje"=>"Asistencia fue confirmada anteriormente", "reservados"=>$reservacion],200);
            }

            $reservado = Reservados::select(
                                    'reservados.nu_reservado',
                                    'programas.nu_programa',
                                    'dt_fecha'
                                    )
                                    ->join('programas', 'programas.nu_programa', 'reservados.nu_programa')
                                    ->where('reservados.nu_reservado',$nu_reservado)
                                    ->where('programas.dt_fecha', date('Y-m-d'))->first();
            if($reservado){
                $reservacion->update(["nu_estatus" => "3"]);
                return response()->json(["intState"=>1, "strMensaje"=>"Se confirmó la asitencia correctamente", "reservados"=>$reservacion],200);    
            }else{
                
                return response()->json(["intState"=>0, "strMensaje"=>"No es una reservación del dia", "reservados"=>""],200);
            }
        }else{
            return response()->json(["intState"=>0, "strMensaje"=>"No se encontro la reservacion", "reservados"=>""],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservados  $reservados
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservados $reservados)
    {
        //
    }
    
    function encryptSello($string, $key) {
        $result = '';
        for($i=0; $i<strlen($string); $i++) {
           $char = substr($string, $i, 1);
           $keychar = substr($key, ($i % strlen($key))-1, 1);
           $char = chr(ord($char)+ord($keychar));
           $result.=$char;
        }
        return base64_encode($result);
    }
    
    function decryptSello($string, $key) {
        $result = '';
        $string = base64_decode($string);
        for($i=0; $i<strlen($string); $i++) {
           $char = substr($string, $i, 1);
           $keychar = substr($key, ($i % strlen($key))-1, 1);
           $char = chr(ord($char)-ord($keychar));
           $result.=$char;
        }
        return $result;
    }
}
