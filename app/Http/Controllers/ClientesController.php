<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class ClientesController extends Controller
{
    /**
     * Obtener todos los clientes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Clientes::all();
    }

    /**
     * Crear Usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->isJson()){
            $dataRequest = request()->all();

            $clientes = Clientes::where('ln_email',$dataRequest['ln_email'])->first();
            
            if($clientes){
                return response()->json(["intState"=>0,"strMensaje"=>"Ya existe el correo","cliente"=>""],200) ;
            }

            $cliente=Clientes::create([
                "ln_nombre" => $dataRequest['ln_nombre'], 
                "ln_apellidos" => $dataRequest['ln_apellidos'], 
                "ln_usuario_cliente" => $dataRequest['ln_usuario_cliente'], 
                "ln_contrasena" => Hash::make($dataRequest['ln_contrasena']), 
                "ln_email" => $dataRequest['ln_email'], 
                "ln_telefono" => $dataRequest['ln_telefono'], 
                "avatar" => $dataRequest['avatar'], 
                "nu_activo" => '1'
            ]);

            if($cliente){
                return response()->json(["intState"=>1,"strMensaje"=>"Se creo correctamente el cliente.","cliente"=>$cliente],200) ;
            }else{
                return response()->json(["intState"=>0,"strMensaje"=>"Verificar proceso.","cliente"=>""],200) ;
            }
        }
        return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","cliente"=>""],200) ;
    }

    /**
     * Login App validando cliente.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        if(request()->isJson()){
            $dataRequest = request()->all();
            $cliente = Clientes::where('ln_email',$dataRequest['ln_email'])->where('nu_activo', '1')->first();

            if($cliente && Hash::check($dataRequest['ln_contrasena'],$cliente->ln_contrasena)){
                return response()->json(["intState"=>1,"strMensaje"=>"Credenciales Correctas","cliente"=>$cliente],200) ;
            }
        }   

        return response()->json(["intState"=>0,"strMensaje"=>"Credenciales Incorrectas","cliente"=>""],200) ;
    }

    public function registerAccount(Request $request){
        
        $clientes = Clientes::where('ln_email',$request->ln_email)->first();
            
        if($clientes){
            return response()->json(["intState"=>0,"strMensaje"=>"Ya existe registrado el correo.","cliente"=>""],200) ;
        }
        
        $cliente=Clientes::create([
            "ln_nombre" => $request->ln_nombre, 
            "ln_apellidos" => $request->ln_apellidos, 
            "ln_usuario_cliente" => $request->ln_usuario_cliente, 
            "ln_contrasena" => Hash::make($request->ln_contrasena), 
            "ln_email" => $request->ln_email, 
            "ln_telefono" => $request->ln_telefono, 
            "avatar" => $request->avatar, 
            "nu_termino" => $request->nu_termino, 
            "nu_activo" => '1'
        ]);

        if($cliente){
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el cliente.","cliente"=>$cliente],200) ;
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verificar proceso.","cliente"=>""],200) ;
        }
   }

}
