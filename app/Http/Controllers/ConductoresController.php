<?php

namespace App\Http\Controllers;

use App\Conductores;
use App\Programas;
use Illuminate\Http\Request;

class ConductoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $conductores = Conductores::where('ln_nombre', 'like', '%'.$dataRequest['ln_nombre'].'%')->orderBy('ln_nombre', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los conductores correctamente.","conductores"=>$conductores],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","conductores"=>""],400) ;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(request()->ajax()){
        //     return response()->json(["intState"=>0,"strMensaje"=>"Credenciales Incorrectas"],200) ;
        // }
        
        //return Conductores::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            //dd($dataRequest);
            $estatus=0;
            if(isset($dataRequest['nu_activo'])){
                $estatus=1;
            }

            $conductor=Conductores::create([
                "ln_nombre" => $dataRequest['ln_nombre'], 
                "ln_apellidos" => $dataRequest['ln_apellidos'], 
                "ln_direccion" => $dataRequest['ln_direccion'], 
                "ln_telefono" => $dataRequest['ln_telefono'], 
                "dt_fecha_nacimiento" => $dataRequest['dt_fecha_nacimiento'], 
                "nu_activo" => $estatus
            ]);
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el conductor: ". $dataRequest['ln_nombre'],"conductor"=>compact('conductor')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","conductor"=>""],400) ;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function show(Conductores $conductores, $nu_conductor)
    {
        if(request()->ajax()){
            $conductor = Conductores::findOrFail($nu_conductor);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","conductor"=>compact("conductor")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar conductor.","conductor"=>""],400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function edit(Conductores $conductores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conductores $conductores, $nu_conductor)
    {
        if(request()->ajax()){
            //$conductores = Conductores::findOrFail($nu_conductor) ;
            $dataRequest = request()->all();

            // if (!isset($dataRequest['ln_nombre'])) {
            //     $dataRequest['ln_nombre'] = '';
            // }

            // if (!isset($dataRequest['ln_apellidos'])) {
            //     $dataRequest['ln_apellidos'] = '';
            // }

            // if (!isset($dataRequest['ln_direccion'])) {
            //     $dataRequest['ln_direccion'] = '';
            // }

            // if (!isset($dataRequest['ln_telefono'])) {
            //     $dataRequest['ln_telefono'] = '';
            // }

            // if (!isset($dataRequest['dt_fecha_nacimiento'])) {
            //     $dataRequest['dt_fecha_nacimiento'] = '';
            // }

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            //$conductores = Conductores::where('nu_conductor',$nu_conductor)->first();
            $conductores = Conductores::findOrFail($nu_conductor);

            //dd($conductores);
            $conductores->update($dataRequest);
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente a: ".$conductores->ln_nombre,"conductor"=>compact('conductores')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","conductor"=>""],400);
        } 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conductores $conductores, $nu_conductor)
    {
        if(request()->ajax()){
            $programas = Programas::where('nu_conductor',$nu_conductor)->first();
            
            if($programas){
                return response()->json(["intState"=>0,"strMensaje"=>"Problemas el conductor esta asociado a una salida, consultar programación."],200);
            }else{
                $conductores = Conductores::findOrFail($nu_conductor);
                $conductores->delete();
                return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente a: ".$conductores->ln_nombre,"conductor"=>$conductores],200);
            }
            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","conductor"=>""],400);
        }
    }

    public function fnCargarComboConductores(){
        $conductores = Conductores::select(['nu_conductor',"ln_nombre"])
                            ->where('nu_activo', '1')
                            ->orderBy('ln_nombre', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$conductores],200);  
    }
}
