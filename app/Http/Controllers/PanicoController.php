<?php

namespace App\Http\Controllers;

use App\Panico;
use Illuminate\Http\Request;

class PanicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Panico::where('nu_activo', '1')
                            ->orderBy('nu_panico', 'ASC')->get();

        if($servicios){
            return response()->json(["intState"=>1,"datos"=>$servicios],200) ;
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"No se encontraron servicios","datos"=>""],400) ;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Panico  $panico
     * @return \Illuminate\Http\Response
     */
    public function show(Panico $panico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Panico  $panico
     * @return \Illuminate\Http\Response
     */
    public function edit(Panico $panico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Panico  $panico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Panico $panico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Panico  $panico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Panico $panico)
    {
        //
    }
}
