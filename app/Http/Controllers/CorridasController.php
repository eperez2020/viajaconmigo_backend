<?php

namespace App\Http\Controllers;

use App\Corridas;
use App\Programas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CorridasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            //$corridas = Corridas::join('destinos', 'nu_destino', 'nu_origen');
            // $corridas = DB::table('user')
            // ->join('destinos', 'nu_destino', 'nu_origen');
            // dd($corridas);
            $corridas = Corridas::select('corridas.*', 'origenes.ln_nombre as nombre_origen', 'llegadas.ln_nombre as nombre_llegada')
                                ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                                ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                                ->NuOrigen($dataRequest['nu_origen'])
                                ->NuDestino($dataRequest['nu_llegada'])
                                ->orderBy('nu_corrida', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron las corridas correctamente.","corridas"=>$corridas],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","corridas"=>""],400) ;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $estatus=0;

            if(isset($dataRequest['nu_activo'])){
                $estatus=1;
            }

            $corrida=Corridas::create([
                'nu_origen'=>$dataRequest['nu_origen'],
                'nu_llegada'=>$dataRequest['nu_llegada'],
                'ln_hora_salida'=>$dataRequest['ln_hora_salida'],
                'ln_hora_llegada'=>$dataRequest['ln_hora_llegada'],
                'dbl_precio'=>$dataRequest['dbl_precio'],
                'nu_activo'=>$estatus
            ]);
            
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el corrida","corrida"=>compact('corrida')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","corrida"=>""],400) ;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corridas  $corridas
     * @return \Illuminate\Http\Response
     */
    public function show(Corridas $corridas, $nu_corrida)
    {
        if(request()->ajax()){
            $corrida = Corridas::findOrFail($nu_corrida);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","corrida"=>compact("corrida")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar corrida.","corrida"=>""],400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Corridas  $corridas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Corridas $corridas, $nu_corrida)
    {
        if(request()->ajax()){
            
            $dataRequest = request()->all();

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            $corrida = Corridas::findOrFail($nu_corrida);

            $corrida->update($dataRequest);
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente","corrida"=>compact('corrida')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","corrida"=>""],400);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Corridas  $corridas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Corridas $corridas, $nu_corrida)
    {
        if(request()->ajax()){
            $programas = Programas::where('nu_corrida',$nu_corrida)->first();
            
            if($programas){
                return response()->json(["intState"=>0,"strMensaje"=>"Problemas la corrida esta asociado a una salida, consultar programación."],200);
            }else{
                $corrida = Corridas::findOrFail($nu_corrida);
                $corrida->delete();
                return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente la corrida","corrida"=>$corrida],200);
            }
            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","corrida"=>""],400);
        }
    }
    

    public function fnCargarComboCorridas(){
        $conductores = Corridas::select(['nu_corrida',DB::raw('CONCAT(origenes.ln_nombre, " - ", llegadas.ln_nombre, " ", corridas.ln_hora_salida) AS corrida')])
                                ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                                ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                                ->where('corridas.nu_activo', '1')
                                ->orderBy('origenes.ln_nombre', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$conductores],200);  
    }

    public function fnCargarComboOrigen(){
        $conductores = Corridas::select(['corridas.nu_origen as id','origenes.ln_nombre as description'])
                                ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                                ->where('corridas.nu_activo', '1')
                                ->groupBy('corridas.nu_origen','origenes.ln_nombre' )
                                ->orderBy('origenes.ln_nombre', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$conductores],200); 
    }

    public function fnCargarComboDestinos(Request $request){
        $conductores = Corridas::select(['corridas.nu_llegada as id','llegadas.ln_nombre as description'])
                                ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                                ->where('corridas.nu_activo', '1')
                                ->where('corridas.nu_origen', $request->nu_origen)
                                ->groupBy('corridas.nu_llegada','llegadas.ln_nombre' )
                                ->orderBy('llegadas.ln_nombre', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$conductores],200); 
    }
}
