<?php

namespace App\Http\Controllers;

use App\Programas;
use App\Reservados;
use Illuminate\Http\Request;

class ProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            $programas = Programas::select('programas.nu_programa',
                                            'programas.nu_corrida',
                                            'programas.dt_fecha',
                                            'programas.nu_vehiculo', 
                                            'programas.nu_conductor', 
                                            'programas.nu_activo',
                                            'corridas.nu_origen',
                                            'corridas.nu_llegada',
                                            'corridas.ln_hora_salida',
                                            'corridas.dbl_precio',
                                            'origenes.ln_nombre as nombre_origen', 
                                            'llegadas.ln_nombre as nombre_llegada',
                                            'vehiculos.ln_placa',
                                            'vehiculos.nu_asiento',
                                            'conductores.ln_nombre as nombre_conductor',
                                            'conductores.ln_apellidos')
                                    ->join('corridas', 'corridas.nu_corrida', 'programas.nu_corrida')
                                    ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                                    ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                                    ->join('vehiculos', 'vehiculos.nu_vehiculo', 'programas.nu_vehiculo')
                                    ->join('conductores', 'conductores.nu_conductor', 'programas.nu_conductor')
                                    ->DtFechaInicio($dataRequest['dt_fecha_inicio'])
                                    ->DtFechaFin($dataRequest['dt_fecha_fin'])
                                    ->NuOrigen($dataRequest['nu_origen'])
                                    ->NuDestino($dataRequest['nu_llegada'])
                                    ->orderBy('dt_fecha', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los programas correctamente.","programas"=>$programas],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","programas"=>""],400) ;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            
            $estatus=0;

            if(isset($request->nu_activo)){
                $estatus=1;
            }

            $programa=Programas::create([
                'nu_corrida'=>$request->nu_corrida,
                'dt_fecha'=>$request->dt_fecha,
                'nu_vehiculo'=>$request->nu_vehiculo,
                'nu_conductor'=>$request->nu_conductor,
                'nu_activo'=>$estatus
            ]);

            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el programa: ". $request->ln_nombre,"programa"=>compact('programa')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","programa"=>""],400) ;            
        }
    }

    public function fnCrearPrograma(Request $request)
    {
        if(request()->ajax()){
            
            $estatus=0;

            if(isset($request->nu_activo)){
                $estatus=1;
            }

            $dtInicio=strtotime("".$request->dt_fecha_inicio);
            $dtFin=strtotime("".$request->dt_fecha_fin);
            
            $dtFecha=0;
            $nu_semanas=$request->nu_semanas;

            foreach($request->nu_corrida as $nu_corrida){
                for($dtFecha=$dtInicio; $dtFecha<=$dtFin; $dtFecha+=86400){
                    $dtFechaSemana=$dtFecha;
                    for ($semanas=0;$semanas<$nu_semanas;$semanas++){

                        $programa=Programas::create([
                            'nu_corrida'=>$nu_corrida,
                            'dt_fecha'=>date("Y-m-d", $dtFechaSemana),
                            'nu_vehiculo'=>$request->nu_vehiculo,
                            'nu_conductor'=>$request->nu_conductor,
                            'nu_activo'=>$estatus
                        ]);

                        $dtFechaSemana +=(86400*7);                        
                        
                    }
                    
                }
            }

            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el programa: ". $request->ln_nombre,"programa"=>compact('programa')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","programa"=>""],400) ;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function show(Programas $programas, $nu_programa)
    {
        if(request()->ajax()){
            $programa = Programas::findOrFail($nu_programa);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","programa"=>compact("programa")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar el programa.","programa"=>""],400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programas $programas, $nu_programa)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            $programa = Programas::findOrFail($nu_programa);

            $programa->update($dataRequest);
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente","programa"=>compact('programa')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","programa"=>""],400);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Programas $programas, $nu_programa)
    {
        if(request()->ajax()){
            $reservados = Reservados::where('nu_programa',$nu_programa)->first();
            
            if($reservados){
                return response()->json(["intState"=>0,"strMensaje"=>"Problemas la salida esta asociado a una reservación, consultar reservaciones."],200);
            }else{
                $programa = Programas::findOrFail($nu_programa);
                $programa->delete();
                return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente la salida","programa"=>$programa],200);
            }
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","programa"=>""],400);
        }
    }

    public function fnObtenerSalidas(Request $request){
        
        $programas = Programas::select(
                    'programas.nu_programa',
                    'programas.nu_corrida',
                    'programas.dt_fecha',
                    'corridas.nu_origen',
                    'corridas.nu_llegada',
                    'corridas.ln_hora_salida',
                    'corridas.ln_hora_llegada',
                    'corridas.dbl_precio',
                    'origenes.ln_nombre as nombre_origen', 
                    'llegadas.ln_nombre as nombre_llegada',
                    'vehiculos.nu_asiento')
                    ->join('corridas', 'corridas.nu_corrida', 'programas.nu_corrida')
                    ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                    ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                    ->join('vehiculos', 'vehiculos.nu_vehiculo', 'programas.nu_vehiculo')
                    ->CountReservados()
                    ->DtFecha($request->dt_fecha)
                    ->NuOrigen($request->nu_origen)
                    ->NuDestino($request->nu_llegada)
                    ->NuActivo(1)
                    ->orderBy('dt_fecha', 'ASC')
                    ->orderBy('corridas.ln_hora_salida', 'ASC')->get();

        if($programas){
            return response()->json(["intState"=>1,"datos"=>$programas],200) ;
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"No se encontraron salidas con lisfiltros seleccionados.","datos"=>""],400) ;
        }
    }

    public function fnObtenerAsistencia(Request $request){
        
        $programas = Programas::select(
                    'programas.nu_programa',
                    'corridas.ln_hora_salida',
                    'corridas.ln_hora_llegada',
                    'corridas.dbl_precio',
                    'origenes.ln_nombre as nombre_origen', 
                    'llegadas.ln_nombre as nombre_llegada',
                    'vehiculos.nu_asiento',
                    'vehiculos.ln_placa',
                    'vehiculos.ln_marca',
                    'vehiculos.ln_modelo'
                    )
                    ->join('corridas', 'corridas.nu_corrida', 'programas.nu_corrida')
                    ->join('destinos as origenes', 'origenes.nu_destino', 'corridas.nu_origen')
                    ->join('destinos as llegadas', 'llegadas.nu_destino', 'corridas.nu_llegada')
                    ->join('vehiculos', 'vehiculos.nu_vehiculo', 'programas.nu_vehiculo')
                    ->CountReservados()
                    ->CountConfirmados()
                    ->DtFecha(date('Y-m-d'))
                    ->NuActivo(1)
                    ->orderBy('dt_fecha', 'ASC')
                    ->orderBy('corridas.ln_hora_salida', 'ASC')->get();

        if($programas){
            return response()->json(["intState"=>1,"datos"=>$programas],200) ;
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"No se encontraron salidas con lisfiltros seleccionados.","datos"=>""],400) ;
        }
    }   


}
