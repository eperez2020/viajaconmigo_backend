<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntos extends Model
{
    protected $fillable = ['nu_corrida', 'ln_nombre', 'nu_tipo', 'nu_activo'];
    protected $primaryKey = 'nu_punto';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeNuCorrida($query, $nu_corrida = '') {
        if (!empty($nu_corrida) and !is_null($nu_corrida)) {
            return $query->where('nu_corrida',  $nu_corrida);
        }
    }

    public function scopeNuTipo($query, $nu_tipo = '') {
       // if (!empty($nu_tipo) and !is_null($nu_tipo)) {
    	    return $query->where('nu_tipo', $nu_tipo);
        //}
    }
    
    public function scopeNuActivo($query, $nu_activo = '') {
        if (!empty($nu_activo) and !is_null($nu_activo)) {
    	    return $query->where('nu_activo', $nu_activo);
        }
    }
}
