<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conductores extends Model
{
    protected $fillable = ['ln_nombre','ln_apellidos','ln_direccion','ln_telefono','dt_fecha_nacimiento','nu_activo'];
    protected $primaryKey = 'nu_conductor';
    protected $hidden = ['updated_at', 'created_at'];
}
