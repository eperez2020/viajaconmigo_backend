<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{
    protected $fillable = ['nu_vehiculo','nu_vin','ln_placa','ln_marca','ln_modelo','nu_anio','nu_asiento','nu_activo'];
    protected $primaryKey = 'nu_vehiculo';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeNuVin($query, $nu_vin = '') {
    	return $query->where('nu_vin', 'like', '%'.$nu_vin.'%');
    }

    public function scopeLnPlaca($query, $ln_placa = '') {
    	return $query->where('ln_placa', 'like', '%'.$ln_placa.'%');
    }

    public function scopeLnMarca($query, $ln_marca = '') {
    	return $query->where('ln_marca', 'like', '%'.$ln_marca.'%');
    }

    public function scopeLnModelo($query, $ln_modelo = '') {
    	return $query->where('ln_modelo', 'like', '%'.$ln_modelo.'%');
    }

    public function scopeNuAnio($query, $nu_anio = '') {
    	return $query->where('nu_anio', 'like', '%'.$nu_anio.'%');
    }

    public function scopeNuActivo($query, $nu_activo = '') {
    	return $query->where('nu_activo', 'like', '%'.$nu_activo.'%');
    }
}
