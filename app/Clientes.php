<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = ["ln_nombre", "ln_apellidos", "ln_usuario_cliente", "ln_contrasena", "ln_email", "ln_telefono", "avatar", "nu_termino", "nu_activo", "remember_token"];
    protected $hidden = ['ln_contrasena'];
}
