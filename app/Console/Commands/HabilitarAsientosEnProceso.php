<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reservados;
use Illuminate\Support\Facades\Log;

class HabilitarAsientosEnProceso extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:habiliarasientos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Habiliar los asientos que se quedaron en un estatus En Proceso de la tabla reservados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Eliminar Asientos con un estatus En Proceso (igual a cero)
        Reservados::where('nu_estatus', '0')
                    ->whereRaw('(TIMESTAMPDIFF(MINUTE, created_at, ?)) >= 10', date('Y-m-d H:i:s'))
                    ->delete();
        Log::info("Proceso Completado: Habilitar Asientos en estatus En Proceso.");
    }
}
