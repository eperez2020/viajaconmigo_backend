<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Programas extends Model
{
    protected $fillable = ['nu_corrida', 'dt_fecha', 'nu_vehiculo', 'nu_conductor', 'nu_activo'];
    protected $primaryKey = 'nu_programa';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeDtFecha($query, $dt_fecha = '') {
        if (!empty($dt_fecha and !is_null($dt_fecha))) {
    	    return $query->where('programas.dt_fecha', '=', $dt_fecha);
        }
    }

    public function scopeDtFechaInicio($query, $dt_fecha = '') {
        if (!empty($dt_fecha and !is_null($dt_fecha))) {
    	    return $query->where('programas.dt_fecha', '>=', $dt_fecha);
        }
    }

    public function scopeDtFechaFin($query, $dt_fecha = '') {
        if (!empty($dt_fecha and !is_null($dt_fecha))) {
    	    return $query->where('programas.dt_fecha', '<=', $dt_fecha);
        }
    }

    public function scopeNuOrigen($query, $nu_origen = '') {
        if (!empty($nu_origen and !is_null($nu_origen))) {
    	    return $query->where('corridas.nu_origen', $nu_origen);
        }
    }
    
    public function scopeNuDestino($query, $nu_llegada = '') {
        if (!empty($nu_llegada) and !is_null($nu_llegada)) {
    	    return $query->where('corridas.nu_llegada', $nu_llegada);
        }
    }

    public function scopeNuActivo($query, $nu_activo = '') {
        if (!empty($nu_activo) and !is_null($nu_activo)) {
    	    return $query->where('programas.nu_activo', $nu_activo);
        }
    }

    public function scopeCountReservados($query) {
        $subReservados = Reservados::select(DB::raw('count(reservados.nu_programa)'))
            ->whereColumn('reservados.nu_programa', 'programas.nu_programa')
            ->where('reservados.nu_estatus','!=', '2') /* 2 canceladas */
            ->groupBy('reservados.nu_programa');
            
        $query->addSelect([
            "cnt_reservados"=>$subReservados
        ]);
    }

    public function scopeCountConfirmados($query) {
        $subReservados = Reservados::select(DB::raw('count(reservados.nu_programa)'))
            ->whereColumn('reservados.nu_programa', 'programas.nu_programa')
            ->where('reservados.nu_estatus', '3') /* 3 asistencia confimada */
            ->groupBy('reservados.nu_programa');
            
        $query->addSelect([
            "cnt_asistencia"=>$subReservados
        ]);
    }
}
