<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $fillable = ['ln_titulo','ln_correo','ln_telefono','nu_activo'];
    protected $primaryKey = 'nu_contacto';
    protected $hidden = ['updated_at', 'created_at'];
}
