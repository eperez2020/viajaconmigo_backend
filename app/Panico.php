<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panico extends Model
{

    protected $fillable = ['ln_servicio','ln_telefono','nu_activo'];
    protected $primaryKey = 'nu_panico';
    protected $hidden = ['updated_at', 'created_at'];

}
