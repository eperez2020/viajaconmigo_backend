<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservados extends Model
{
    protected $fillable = ['nu_programa', 'nu_cliente', 'nu_asiento', 'ln_nombre', 'nu_salida', 'nu_llegada', 'nu_puntuacion', 'ln_comentario', 'nu_estatus', 'ln_qr', 'updated_at', 'created_at'];
    protected $primaryKey = 'nu_reservado';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeNuReservacion($query, $nu_reservado = '') {
        if (!empty($nu_reservado) and !is_null($nu_reservado)) {
    	    return $query->where('reservados.nu_reservado', $nu_reservado);
        }
    }

    public function scopeNuCliente($query, $nu_cliente = '') {
        if (!empty($nu_cliente) and !is_null($nu_cliente)) {
    	    return $query->where('reservados.nu_cliente', $nu_cliente);
        }
    }
    
}
