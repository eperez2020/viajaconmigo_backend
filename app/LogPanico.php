<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPanico extends Model
{
    protected $fillable = ['nu_panico','nu_cliente'];
    protected $primaryKey = 'nu_log_panico';
    protected $hidden = ['updated_at', 'created_at'];
}
