<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinos extends Model
{
    protected $fillable = ['ln_nombre', 'ln_codigo', 'nu_activo'];
    protected $primaryKey = 'nu_destino';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeLnNombre($query, $ln_nombre = '') {
    	return $query->where('ln_nombre', 'like', '%'.$ln_nombre.'%');
    }

    public function scopeLnCodigo($query, $ln_codigo = '') {
    	return $query->where('ln_codigo', 'like', '%'.$ln_codigo.'%');
    }

    public function scopeNuActivo($query, $nu_activo = '') {
    	return $query->where('nu_activo', 'like', '%'.$nu_activo.'%');
    }
}
