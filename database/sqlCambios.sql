/**
 * Catalogo Moneda, 2020/05/04
 */
ALTER TABLE satMoneda ADD sn_activo tinyint(4) NOT NULL DEFAULT '1';

/**
 * Catalogo Tipo de Comprobante, 2020/05/04
 */
ALTER TABLE satTipoComprobante MODIFY COLUMN nu_valorMaximo double NOT NULL DEFAULT 0;

/**
 * Catalogo clientes, 2020/05/06
 */
ALTER TABLE clientes DROP sn_curp;
ALTER TABLE proveedores DROP sn_curp;