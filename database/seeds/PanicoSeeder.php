<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PanicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('panicos')->insert([
            'ln_servicio' => 'Emergencia',
            'ln_telefono' => '911',
            'nu_activo' => '1',
            'created_at' => '2020-08-01',
            'updated_at' => '2020-08-01'
        ]);
    }
}
