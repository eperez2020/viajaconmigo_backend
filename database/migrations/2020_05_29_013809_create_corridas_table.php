<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corridas', function (Blueprint $table) {
            $table->bigIncrements('nu_corrida');
            $table->bigInteger('nu_origen');
            $table->bigInteger('nu_llegada');
            $table->char('ln_hora_salida', 5);
            $table->char('ln_hora_llegada', 5);
            $table->double('dbl_precio', 9, 2);
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corridas');
    }
}
