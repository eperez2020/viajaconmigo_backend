<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('nu_cliente');
            $table->string('ln_nombre', 40);
            $table->string('ln_apellidos', 70);
            $table->string('ln_usuario_cliente', 20);
            $table->char('ln_contrasena', 60);
            $table->string('ln_email', 100)->unique();
            $table->string('ln_telefono', 25);
            $table->binary('avatar');
            $table->smallInteger('nu_termino')->comment('1 aceptacion a los terminos y condiciones de la app');
            $table->smallInteger('nu_activo');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
