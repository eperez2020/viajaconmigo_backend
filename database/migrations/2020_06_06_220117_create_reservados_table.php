<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservados', function (Blueprint $table) {
            $table->bigIncrements('nu_reservado');
            $table->integer('nu_programa');
            $table->integer('nu_cliente');
            $table->smallInteger('nu_asiento');
            $table->string('ln_nombre');
            $table->smallInteger('nu_salida');
            $table->smallInteger('nu_llegada');
            $table->smallInteger('nu_puntuacion');
            $table->text('ln_comentario');
            $table->smallInteger('nu_estatus')->comment("0-proceso en reservacion, 1-confirmacion de la reservacion, 2 reservacion cancelada despues de confirmar, 3 asistencia confirmada");
            $table->text('ln_qr');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservados');
    }
}
