<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->bigIncrements('nu_vehiculo');
            $table->string('nu_vin', 20);
            $table->string('ln_placa', 15);
            $table->string('ln_marca', 50);
            $table->string('ln_modelo', 50);
            $table->char('nu_anio', 4);
            $table->smallInteger('nu_asiento');
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
