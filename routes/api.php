<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

 /* RUTAS CLIENTE LOGIN Y CREARCUENTA */
Route::get('loginapp', "ClientesController@show");
Route::post('registrarcuenta', "ClientesController@registerAccount");
Route::get('loginconductor', "UsersController@fnLoginConductor");

/* RUTA CARGAR COMBO CORRIDAS */
Route::get('comboOrigen', "CorridasController@fnCargarComboOrigen");

/* RUTA CARGAR COMBO DETINO RECIBO NU_ORIGEN */
Route::get('comboDestinos', "CorridasController@fnCargarComboDestinos");

/* RUTA CARGAR COMBO DE PUNTOS */
Route::get('comboPuntosReunion', "PuntosController@fnComboPuntosReunion");

/* RUTA OBTENER LAS SALIDAS PROGRAMADAS*/
Route::get('obtenerSalidas', "ProgramasController@fnObtenerSalidas");

/* RUTA OBTENER LAS SALIDAS PROGRAMADAS*/
Route::get('obtenerAsientosReservados', "ReservadosController@index");

/* RUTA OBTENER INFO DE LA RESERVACIONes QUES ESTA O HIZO EL CLIENTE*/
Route::get('obtenerHistorialReservaciones', "ReservadosController@fnHistorialReservaciones");

/* RUTA PARA GENERAR RESERVACION */
Route::post('generarReservacion', "ReservadosController@fnGenerarReservacion");

/* RUTA PARA CANCELAR RESERVACION */
Route::post('cancelarReservacion', "ReservadosController@fnCancelarReservacion");

/* RUTA PARA CALIFICAR RESERVACION */
Route::post('calificarReservacion', "ReservadosController@fnCalificacionReservacion");

/* RUTA PARA CONFIRMAR ASISTENCIA DEL CLIENTE */
Route::post('confirmarAsistencia', "ReservadosController@fnConfirmarAsistencia");

/* RUTA PARA CONFIRMAR Reservacion */
Route::post('confirmarReservacion', "ReservadosController@fnConfirmarReservacion");

Route::get('obtenerAsistencia', "ProgramasController@fnObtenerAsistencia");

Route::get('obtenerServiciosPanico', "PanicoController@index");

Route::post('logPanico', "LogPanicoController@store");

/* RUTA OBTENER INFO DE CONTACTO PARA SOPORTE */
Route::get('obtenerContactoSoporte', "ContactoController@fnObtenerContactoSoporte");
