<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

session_start();

/**
 * Sin Controlador
 */
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('/home', function () {
        return view('abcConductores');
    });

    /* RUTAS DE CONDUCTORES */
    Route::get('/abcConductores', function () {
        return view('abcConductores');
    });
    Route::resource('Conductores','ConductoresController');
    Route::get('/fnCargarComboConductores', "ConductoresController@fnCargarComboConductores");


    /* RUTAS DE VEHICULOS */
    Route::get('/abcVehiculos', function () {
        return view('abcVehiculo');
    });
    Route::resource('Vehiculos','VehiculosController');
    Route::get('/fnCargarComboVehiculos', "VehiculosController@fnCargarComboVehiculos");


    /* RUTAS DE DESTINOS */
    Route::get('/abcDestinos', function () {
        return view('abcDestinos');
    });
    Route::resource('Destinos','DestinosController');
    Route::get('/fnCargarComboDestinos', "DestinosController@fnCargarComboDestinos");


    /* RUTAS DE CORRIDAS */
    Route::get('/abcCorridas', function () {
        return view('abcCorridas');
    });
    Route::resource('Corridas','CorridasController');
    Route::get('/fnCargarComboCorridas', "CorridasController@fnCargarComboCorridas");


    /* RUTAS DE PUNTOS */
    Route::resource('Puntos','PuntosController');
    

    /* RUTAS DE PROGRAMACION */
    Route::get('/Programacion', function () {
        return view('programas');
    });
    Route::resource('Programas','ProgramasController');
    Route::post('/fnCrearPrograma', "ProgramasController@fnCrearPrograma");
    

    Route::get('/abcUsuarios', function () {
        return view('abcUsuarios');
    });
    Route::resource('Users','UsersController');

    // Route::get('qr-code', function () 
    // {
    // return QRCode::text('QR Code Generator for Laravel!')->png();    
    // });
});